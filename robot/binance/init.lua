local class = require 'metaclass'
local http = require 'http.client'
local merge = require 'robot.utils'.merge
local json = require 'json'
local background = require 'background'
local struct = require 'robot.binance.struct'
local fiber = require 'fiber'
local config = require 'config'

local log = require 'log'
local orm = require 'robot.orm'


local Binance = class "Binance"
function Binance:constructor()
	assert(self.url, "binance: url is required")

	if self.background then
		self.cleaner_f = background {
			name = 'binance_f/cleaner',
			wait = 'rw',
			restart = true,
			run_interval = 60,
			func = function()
				self:clear(-60)
			end,
		}
		self.fibers = {}
		self.rlimit_ch = require'fiber'.channel(self.background.burst or 3)
		self.rlimit_f = background {
			name = 'binance_f/rlimit',
			wait = 'rw',
			restart = true,
			run_interval = 1/(self.background.rps or 3),
			func = function()
				self.rlimit_ch:put(true, 0)
			end,
		}
		self.rlimit_fp_ch = fiber.channel(config.get('app.binance.background.firstpage.burst', 1))
		self.rlimit_fp_f = background {
			name = 'bunance_f/rlimit_fp',
			wait = 'rw',
			restart = true,
			run_interval = 1 / config.get('app.binance.background.firstpage.rps', 1),
			func = function()
				self.rlimit_fp_ch:put(true, 0)
			end,
		}
		for _, pair in pairs(self.background.pairs) do
			self.fibers[pair] = background {
				name = 'binance_f/'..pair,
				wait = 'rw',
				restart = true,
				run_interval = self.background.interval or 600,
				setup = function(ctx)
					ctx.pair = pair
					fiber.self().storage.rlimit = self.rlimit_ch
				end,
				func = function(ctx)
					local fiat, asset = ctx.pair:match("(%S+)_(%S+)")
					self:download{ asset = asset, fiat = fiat, tradeType = 'BUY' }
					self:download{ asset = asset, fiat = fiat, tradeType = 'SELL' }
				end,
			}
			self.fibers[pair..'_fp'] = background {
				name = 'binance_f/'..pair..'_fp',
				wait = 'rw',
				restart = true,
				run_interval = 3,
				setup = function(ctx)
					fiber.self().storage.rlimit = self.rlimit_fp_ch
					ctx.pair = pair
				end,
				func = function(ctx)
					local fiat, asset = ctx.pair:match("(%S+)_(%S+)")
					self:firstPage{ asset = asset, fiat = fiat, tradeType = 'BUY' }
					self:firstPage{ asset = asset, fiat = fiat, tradeType = 'SELL' }
				end,
			}
		end
	end
end

function Binance:refresh(req, opts)
	if not opts.nolimit then
		fiber.self().storage.rlimit:get()
	end
	opts.nolimit = nil
	local r = http.post(self.url,
		json.encode(merge(req or {}, { page = 1, rows = 10, asset = 'USDT', tradeType = 'BUY', fiat = 'RUB' })),
		merge(opts or {}, { timeout = self.timeout, headers = { ['content-type'] = 'application/json' } })
	)
	if r.status ~= 200 then
		return nil, ("binance http failed: not 200: %s %s"):format(r.status, r.body)
	end
	local res = struct.response(json.decode(r.body))

	if type(res.data) ~= 'table' then
		return nil, ("binance response failed: %s: %s %s"):format(res.status, res.message, res.messageDetail)
	end

	for _, order in pairs(res.data) do
		order.adv = struct.Advertise(order.adv)
		order.advertiser = struct.Advertiser(order.advertiser)
		order.adv._data.advertiser = order.advertiser:getpk()
	end

	return res
end

function Binance:firstPage(req)
	req = req or {}
	req.page = 1
	local started = fiber.time()
	local ads = self:refresh(req, { timeout = 10 })
	for _, ad in ipairs(ads.data) do
		orm.space.binance_ads:replace(ad.adv:totuple())
		orm.space.binance_advs:replace(ad.advertiser:totuple())
		ad.adv:export():save()
	end

	log.info("firstpage %s - %.2fs", json.encode(req), fiber.time()-started)
end

function Binance:download(req)
	req = req or {}
	req.page = 1
	local count = 0
	repeat
		local ads = self:refresh(req, { timeout = 10 })
		for _, ad in ipairs(ads.data) do
			orm.space.binance_ads:replace(ad.adv:totuple())
			orm.space.binance_advs:replace(ad.advertiser:totuple())
			ad.adv:export():save()
		end
		count = count + #ads.data
		req.page = req.page + 1
		log.info("D: %s [%d/%d] %.2f%%", json.encode(req), count, ads.total, count/ads.total*100)
	until count >= ads.total or req.page >= 10
	return count
end

function Binance:clear(epoch)
	epoch = epoch or -3600
	if epoch < 0 then
		epoch = os.time()+epoch
	end

	require 'moonwalker' {
		space = orm.space.binance_ads.space,
		examine = function(t)
			return t.updated_at < epoch
		end,
		actor = function(t)
			return orm.space.binance_ads.tuple(t):delete()
		end
	}
end

return Binance