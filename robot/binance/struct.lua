local val = require 'robot.val'
local Fabric = require 'robot.fabric'

local validators = {
	adv = val.req('table', {
		advNo = 'string',
		asset = 'string',
		assetScale = 'number',
		fiatUnit = 'string',
		amountAfterEditing = '?string',
		commissionRate = val.opt(val.decimal),
		dynamicMaxSingleTransAmount = val.opt(val.decimal),
		dynamicMaxSingleTransQuantity = val.opt(val.decimal),
		fiatScale = '?number',
		initAmount = val.decimal,
		maxSingleTransAmount = val.decimal,
		maxSingleTransQuantity = val.decimal,
		minSingleTransAmount = val.decimal,
		minSingleTransQuantity = val.decimal,
		price = val.decimal,
		priceScale = 'number',
		surplusAmount = val.decimal,
		tradableQuantity = val.decimal,
		tradeType = 'string',
		tradeMethods = val.list(val.idator{
			payType = 'string',
		})
	}),
	advertiser = val.req('table', {
		userNo = 'string',
		monthOrderCount = val.integer,
		monthFinishRate = 'number',
		userGrade = 'number',
		nickName = 'string',
		advConfirmTime = '?number',
		userType = 'string',
		userIdentity = '?string',
	})
}

local Advertise = Fabric { name = 'Binance.Advertise' } : subclass {
	folding = {
		tradeMethods = function(value)
			return require 'fun'.iter(value)
				:map(function(v)
					if type(v) == 'table' then return v.payType end
					return v
				end)
				:totable()
		end,
	},
	validator = validators.adv,
	schema = {
		name = 'binance_ads',
		format = {
			{ name = 'advNo',                         type = 'string' },
			{ name = 'updated_at',                    type = 'number', auto = 'time' },
			{ name = 'asset',                         type = 'string' },
			{ name = 'assetScale',                    type = 'number' },
			{ name = 'fiatUnit',                      type = 'string' },
			{ name = 'amountAfterEditing',            type = 'string', is_nullable = true },
			{ name = 'commissionRate',                type = 'decimal', is_nullable = true },
			{ name = 'dynamicMaxSingleTransAmount',   type = 'decimal', is_nullable = true },
			{ name = 'dynamicMaxSingleTransQuantity', type = 'decimal', is_nullable = true },
			{ name = 'fiatScale',                     type = 'number', is_nullable = true },
			{ name = 'initAmount',                    type = 'decimal' },
			{ name = 'maxSingleTransAmount',          type = 'decimal' },
			{ name = 'maxSingleTransQuantity',        type = 'decimal' },
			{ name = 'minSingleTransAmount',          type = 'decimal' },
			{ name = 'minSingleTransQuantity',        type = 'decimal' },
			{ name = 'price',                         type = 'decimal' },
			{ name = 'priceScale',                    type = 'integer', is_nullable = true },
			{ name = 'surplusAmount',                 type = 'decimal', is_nullable = true },
			{ name = 'tradableQuantity',              type = 'decimal', is_nullable = true },
			{ name = 'tradeType',                     type = 'string' },
			{ name = 'tradeMethods',                  type = 'array'  },
			{ name = '_data',                         type = 'map'    },
		},
		primary = {'advNo'},
		secondaries = {
			{ name = 'asset', parts = { 'asset', 'tradeType', 'price', 'advNo' } },
			{ name = 'fiat', parts = { 'fiatUnit', 'tradeType', 'price', 'advNo' } },
			{ name = 'updated', parts = { 'updated_at', 'advNo' } },
		},
	},
}

local Order = require 'robot.Order'

function Advertise:export()
	self:fold()
	return Order {
		market = 'binance',
		updated_at = self.updated_at,
		order_no = self.advNo,
		crypto = self.asset,
		fiat = self.fiatUnit,
		price = self.price,
		trade = self.tradeType,
		minFiatAmount = self.minSingleTransAmount,
		maxFiatAmount = self.maxSingleTransAmount,
		minCryptoAmount = self.minSingleTransQuantity,
		maxCryptoAmount = self.maxSingleTransQuantity,
		payMethods = self.tradeMethods,
	}
end

local Advertiser = Fabric { name = 'Binance.Advertiser' } : subclass {
	validator = validators.advertise,
	schema = {
		name = 'binance_advs',
		format = {
			{ name = 'userNo',          type = 'string'  },
			{ name = 'updated_at',      type = 'number', auto = 'time' },
			{ name = 'monthOrderCount', type = 'integer' },
			{ name = 'monthFinishRate', type = 'number'  },
			{ name = 'userGrade',       type = 'number'  },
			{ name = 'nickName',        type = 'string'  },
			{ name = 'advConfirmTime',  type = 'number', is_nullable = true },
			{ name = 'userType',        type = 'string', is_nullable = true },
			{ name = 'userIdentity',    type = 'string', is_nullable = true },
			{ name = '_data',           type = 'map' },
		},
		primary = { 'userNo' },
		secondaries = {
			{ name = 'orderCount', parts = {'monthOrderCount', 'userNo'} },
			{ name = 'finishRate', parts = {'monthFinishRate', 'userNo'} },
		},
	},
}

local response = val.idator {
	code = 'string',
	message = '?string',
	messageDetail = '?string',
	total = 'number',
	success = 'boolean',

	data = val.opt('table', val.list(val.idator {
		adv = validators.adv,
		advertiser = validators.advertiser,
	}))
}


return {
	response = response,

	Advertiser = Advertiser,
	Advertise = Advertise,
}