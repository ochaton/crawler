local val = require 'val'

function val.list(...)
	local nexters = {...}
	return function(v, name)
		if type(v) ~= 'table' then
			error{ 'invalid', name..' must be a table' }
		end
		if #v == 0 and not next(v) then
			return v
		end
		if next(v, #v) ~= nil then
			error{ 'invalid', name..' must be a list of items' }
		end

		for i = 1, #v do
			for _, n in ipairs(nexters) do
				v[i] = n(v[i])
			end
		end
		return v
	end
end

function val.decimal(value, name)
	local ok, v = pcall(require'decimal'.new, value)
	if not ok then
		error{'invalid', name..' is not decimal value'}
	end
	return v
end

function val.integer(value, name)
	if type(value) == 'cdata' then
		local t = require 'ffi'.typeof(value)
		if t ~= 'ctype<uint64_t>' and t ~= 'ctype<int64_t>' then
			error{ 'invalid', name..' must be integer but is '..t }
		end
		return value
	end
	local v = tonumber(value)
	if type(v) ~= 'number' then
		error{ 'invalid', name..' is not a number '..type(value) }
	end
	if math.floor(value) ~= value then
		error{ 'invalid', name..' must be integer but has floating part' }
	end
	return value
end

function val.enum(choices)
	local fun = require 'fun'
	local kv = fun.zip(choices, fun.ones()):tomap()
	local str = table.concat(fun.map(tostring, choices):totable(), ',')
	return function(value, name)
		if not kv[value] then
			error{'invalid', name..' value must be one of '..str.. ' got '..tostring(value)}
		end
		return value
	end
end

val.one_of = val.enum

return val