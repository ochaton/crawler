local class = require 'metaclass'
local val = require 'robot.val'
local orm = require 'robot.orm'

local Hider = class "Robot.Fabric.Hider"
function Hider:constructor()
	error("Hider cannot be instantiated")
end

local Fabric = class "Robot.Fabric" : prototype {
}

function Fabric:constructor()
	self.class = class(self.name):inherits(require 'robot.base')
end

function Fabric:subclass(proto)
	local space
	if proto.schema then
		if proto.schema.name then
			space = orm.space[proto.schema.name]
			self.class.methods._space = space
			proto.schema.class = self.class
			space:build(proto.schema.name, proto.schema, orm)
		end
	end

	if proto.hide then
		for _, method in ipairs(proto.hide) do
			if not Hider.methods[method] then
				Hider.methods[method] = function() end
			end
			self.class.methods[method] = Hider.methods[method]
		end
	end

	return self.class:prototype {
		folding = proto.folding,
	}
end

function Fabric:build_validator(schema)
	local val_schema = {}
	for _, f in ipairs(schema.format) do
		if f.is_nullable or f.auto or f.name:sub(1, 1) == '_' then
			if type(self.typemap[f.type]) == 'string' then
				val_schema[f.name] = '?'..self.typemap[f.type]
			else
				val_schema[f.name] = val.opt(self.typemap[f.type])
			end
		else
			val_schema[f.name] = val.req(self.typemap[f.type])
		end
	end
	return val.idator(val_schema)
end

return Fabric