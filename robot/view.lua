local V = {}

local fun = require 'fun'

local function deep_get(t, path)
	local k = table.remove(path, 1)
	if k == nil then return t end
	if t[k] == nil then return nil end
	return deep_get(t[k], path)
end

local cached_func = {}
local cache = function(s)
	local f = cached_func[s] or loadstring(s)()
	if not cached_func[s] then
		cached_func[s] = f
	end
	return f
end

function V.q(args)
	local filter = {}
	local space = args.SPACE
	for key in pairs(args) do
		if space.fmap[key] then
			table.insert(filter, ("v[%q] == %q"):format(key, args[key]))
		end
	end

	filter = { table.concat(filter, " and ") }
	table.insert(filter, 1, "return function(v) return ")
	table.insert(filter, "end")


	filter = table.concat(filter, " ")
	local iter
	if args.ITERATOR then
		iter = args.ITERATOR
	else
		iter = space:pairs(nil, {raw=true, yield=1000})
	end
	local iter = iter
		:grep(cache(filter))
		:grep(args.FILTER or fun.op.truth)

	if not args.RAW then
		iter = iter:map(space.tuple)
	end

	local rows = iter:totable()

	if args.ORDER then
		for i = #args.ORDER, 1, -1 do
			local field = args.ORDER[i]
			if args.DESC then
				table.sort(rows, function(a, b) return a[field] > b[field] end)
			else
				table.sort(rows, function(a, b) return a[field] < b[field] end)
			end
		end
	end

	if args.LIMIT then
		rows = fun.iter(rows):take(args.LIMIT):totable()
	end

	if args.QUERY and args.QUERY ~= '*' then
		if type(args.QUERY) ~= 'table' then
			args.QUERY = { args.QUERY }
		end
		local qmap = fun.zip(args.QUERY, fun.ones()):tomap()
		rows = fun.iter(rows):map(function(v)
			local r = {}
			for k in pairs(qmap) do
				if k:find('.') then
					local val = deep_get(v, k:split('.'))
					local path = k:split('.')

					local p = r
					for i = 1, #path-1 do
						local f = path[i]
						p[f] = p[f] or {}
						p = p[f]
					end
					p[path[#path]] = val
				else
					r[k] = v[k]
				end
			end
			return r
		end)
		:totable()
	end

	return rows
end


return V
--[==[

	robot.binance.q("binance_ads")
		.where[[ .asset == "USDT" and .fiatUnit == "RUB" and .tradeType == "SELL" and .initAmount < 10000 ]]
		.join("binance_advs", "_data.advertiser", "adv")
		.where[[ .adv.monthFinishRate > 0.90 and .adv.monthOrderCount > 100 ]]
		.orderBy({".price", ".adv.monthOrderCount"})
		.limit(5)

]==]