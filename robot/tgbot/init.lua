local class = require 'metaclass'
local clock = require 'clock'
local orm = require 'robot.orm'

local Bot = class "Robot.TG" : inherits(require 'tg.bot')

function Bot:on_update(u)
	local s = clock.time()
	local text = (u.message or {}).text or ""
	local op, asset, fiat = text:match('^(%S+)[ ]+([^/]+)/([^/]+)$')
	if not asset or not fiat or not ({SELL=true,BUY=true})[op] then
		return self.tgapi:sendMessage {
			chat_id = u.message.chat.id,
			text = 'Send exchage pair (ex. BUY USDT/RUB, SELL BTC/USD)',
		}
	end
	local ord = ({SELL='DESC',BUY='ASC'})[op]
	local res = {}
	for _, m in ipairs{"binance","bizlato"} do
		local r = robot.q {
			SPACE = orm.space.orders,
			ITERATOR = orm.space.orders.index.updated:pairs(s-60, {iterator="GE", raw=true}),
			RAW = true,
			market = m,
			crypto = asset,
			fiat = fiat,
			trade = op,
			LIMIT = 1,
			ORDER = {"price"},
			DESC = ord == 'DESC',
			QUERY = {
				"market",
				"crypto",
				"fiat",
				"order_no",
				"price",
				"minFiatAmount",
				"maxFiatAmount",
				"maxCryptoAmount",
				"payMethods",
				"updated_at",
			},
		}
		table.insert(res, r[1])
	end

	local response = {}
	for _, r in ipairs(res) do
		table.insert(response,
			("%s: %s UTC (+%ss)\nQ: %.3fms\n%s %s/%s: %s\n%s - %s\nAvailable: %s %s\nFee: %s\n\n%s\n"):format(
			r.market,
			os.date('!%F %T', r.update_at), os.time()-r.updated_at,
			(clock.time()-s) * 1000,
			op, r.crypto, r.fiat, r.price,
			r.minFiatAmount, r.maxFiatAmount,
			r.maxCryptoAmount, r.crypto,
			r.fee or '?',
			table.concat(r.payMethods, ",")
		))
	end
	self.tgapi:sendMessage{
		chat_id = u.message.chat.id,
		text = table.concat(response, "\n")
	}
end

function Bot:run()
	self:on('update.bot', self.on_update)
	self:super().methods.run(self)
end

return Bot
