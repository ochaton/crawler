local class = require 'metaclass'
local orm = require 'robot.orm'

local Order = class "Order" : inherits(require 'orm.Tuple') : prototype {
	_space = orm.space.orders,
}

orm.space.orders:build('orders', {
	class = Order,
	format = {
		{ name = 'market',                  type = 'string'  },
		{ name = 'updated_at',              type = 'number', auto = 'time' },
		{ name = 'order_no',                type = 'string'  },
		{ name = 'crypto',                  type = 'string'  },
		{ name = 'fiat',                    type = 'string'  },
		{ name = 'price',                   type = 'decimal' },
		{ name = 'trade',                   type = 'string'  }, -- BUY/SELL
		{ name = 'minFiatAmount',           type = 'decimal' },
		{ name = 'maxFiatAmount',           type = 'decimal' },
		{ name = 'minCryptoAmount',         type = 'decimal' },
		{ name = 'maxCryptoAmount',         type = 'decimal' },
		{ name = 'payMethods',              type = 'array'   },
	},
	primary = { 'market', 'order_no' },
	secondaries = {
		{ name = 'crypto',  parts = { 'crypto', 'trade', 'updated_at', 'price', 'order_no', 'market' } },
		{ name = 'fiat',    parts = { 'fiat', 'trade', 'updated_at', 'price', 'order_no', 'market' } },
		{ name = 'updated', parts = { 'updated_at', 'market', 'order_no' } },
	},
}, orm)

function Order:save()
	return self._space:replace(self)
end

return Order