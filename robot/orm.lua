box.spacer = require 'spacer' . new {
	migrations = app_root()..'/migrations/',
	global_ft = true,
	keep_obsolete_spaces = true,
	keep_obsolete_indexes = true,
}
local orm = require 'orm' {
	create_if_not_exists = true,
	spacer = box.spacer,
	automigrate = true,
}
package.loaded[...] = orm
rawset(_G, 'orm', orm)
return orm