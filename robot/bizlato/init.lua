local class = require 'metaclass'
local http = require 'http.client'
local merge = require 'robot.utils'.merge
local json = require 'json'
local background = require 'background'
local struct = require 'robot.bizlato.struct'
local fiber = require 'fiber'
local config = require 'config'

local log = require 'log'
local orm = require 'robot.orm'


local Bizlato = class "Bizlato"
function Bizlato:constructor()
	assert(self.url, "bizlato: url is required")

	if self.background then
		self.cleaner_f = background {
			name = 'bizlato_f/cleaner',
			wait = 'rw',
			restart = true,
			run_interval = 60,
			func = function()
				self:clear(-60)
			end,
		}
		self.fibers = {}
		self.rlimit_ch = require'fiber'.channel(self.background.burst or 3)
		self.rlimit_f = background {
			name = 'bizlato_f/rlimit',
			wait = 'rw',
			restart = true,
			run_interval = 1/(self.background.rps or 3),
			func = function()
				self.rlimit_ch:put(true, 0)
			end,
		}
		self.rlimit_fp_ch = fiber.channel(config.get('app.bizlato.background.firstpage.burst', 1))
		self.rlimit_fp_f = background {
			name = 'bunance_f/rlimit_fp',
			wait = 'rw',
			restart = true,
			run_interval = 1 / config.get('app.bizlato.background.firstpage.rps', 1),
			func = function()
				self.rlimit_fp_ch:put(true, 0)
			end,
		}
		for _, pair in pairs(self.background.pairs) do
			self.fibers[pair] = background {
				name = 'bizlato_f/'..pair,
				wait = 'rw',
				restart = true,
				run_interval = self.background.interval or 600,
				setup = function(ctx)
					ctx.pair = pair
					fiber.self().storage.rlimit = self.rlimit_ch
				end,
				func = function(ctx)
					local currency, cryptocurrency = ctx.pair:match("(%S+)_(%S+)")
					self:download{ cryptocurrency = cryptocurrency, currency = currency }
					self:download{ cryptocurrency = cryptocurrency, currency = currency }
				end,
			}
			self.fibers[pair..'_fp'] = background {
				name = 'bizlato_f/'..pair..'_fp',
				wait = 'rw',
				restart = true,
				run_interval = 3,
				setup = function(ctx)
					fiber.self().storage.rlimit = self.rlimit_fp_ch
					ctx.pair = pair
				end,
				func = function(ctx)
					local currency, cryptocurrency = ctx.pair:match("(%S+)_(%S+)")
					self:download{ cryptocurrency = cryptocurrency, currency = currency }
					self:download{ cryptocurrency = cryptocurrency, currency = currency }
				end,
			}
		end
	end
end

function Bizlato:refresh(req, opts)
	if not opts.nolimit then
		fiber.self().storage.rlimit:get()
	end
	opts.nolimit = nil
	local url = ('%s?skip=%d&limit=20'..
			'&cryptocurrency=%s&currency=%s&type=%s'..
			'&isOwnerVerificated=true&isOwnerTrusted=%s&isOwnerActive=%s&lang=en'):format(
		self.url, req.skip or 0,
		req.cryptocurrency or 'USDT', req.currency or 'RUB', req.type or 'purchase',
		req.isOwnerTrusted or false, req.isOwnerActive or false
	)
	local r = http.get(url,
		merge(opts or {}, { timeout = self.timeout, headers = { ['content-type'] = 'application/json' } })
	)
	if r.status ~= 200 then
		return nil, ("bizlato http failed: not 200: %s %s"):format(r.status, r.body)
	end
	local res = struct.response(json.decode(r.body))

	if type(res.data) ~= 'table' then
		return nil, ("bizlato response failed: %s: %s %s"):format(res.status, res.message, res.messageDetail)
	end

	for i, order in pairs(res.data) do
		res.data[i] = struct.Advertise(order)
	end

	return res
end

function Bizlato:firstPage(req)
	req = req or {}
	req.limit = 20
	local started = fiber.time()
	local ads = self:refresh(req, { timeout = 10 })
	for _, adv in ipairs(ads.data) do
		orm.space.bizlato_ads:replace(adv:totuple())
		adv:export():save()
	end

	log.info("firstpage %s - %.2fs", json.encode(req), fiber.time()-started)
end

function Bizlato:download(req)
	req = req or {}
	req.limit = req.limit or 20
	local count = 0
	repeat
		local ads = self:refresh(req, { timeout = 10 })
		for _, adv in ipairs(ads.data) do
			orm.space.bizlato_ads:replace(adv:totuple())
			adv:export():save()
		end
		count = count + #ads.data
		req.skip = count
		log.info("D: %s [%d/%d] %.2f%%", json.encode(req), count, ads.total, count/ads.total*100)
	until count >= ads.total or req.skip >= 200
	return count
end

function Bizlato:clear(epoch)
	epoch = epoch or -3600
	if epoch < 0 then
		epoch = os.time()+epoch
	end

	require 'moonwalker' {
		space = orm.space.bizlato_ads.space,
		examine = function(t)
			return t.updated_at < epoch
		end,
		actor = function(t)
			return orm.space.bizlato_ads.tuple(t):delete()
		end
	}
end

return Bizlato