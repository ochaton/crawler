local val = require 'robot.val'
local Fabric = require 'robot.fabric'

local validators = {
	order = val.idator {
		id                  = 'number',
		type                = 'string',
		cryptocurrency      = 'string',
		currency            = 'string',
		rate                = val.decimal,
		owner               = 'string',
		ownerLastActivity   = val.integer,
		limitCurrency       = val.req('table', {
			min = val.decimal,
			max = val.decimal,
			realMax = val.opt(val.decimal),
		}),
		limitCryptocurrency = val.req('table', {
			min = val.decimal,
			max = val.decimal,
			realMax = val.opt(val.decimal),
		}),
		paymethod = val.req('table', {
			id = 'number',
			name = 'string',
		}),
		paymethodId         = 'number',
	},
}

local Advertise = Fabric { name = 'Bizlato.Advertise' } : subclass {
	hide = {},
	folding = {
		paymethod = function(value)
			if type(value) == 'table' then
				return value.name
			end
			return value
		end,
	},

	schema = {
		name = 'bizlato_ads',
		format = {
			{ name = 'id',                            type = 'number'  },
			{ name = 'type',                          type = 'string'  },
			{ name = 'updated_at',                    type = 'number', auto = 'time' },
			{ name = 'cryptocurrency',                type = 'string'  },
			{ name = 'currency',                      type = 'string'  },
			{ name = 'rate',                          type = 'decimal' },
			{ name = 'owner',                         type = 'string'  },
			{ name = 'ownerLastActivity',             type = 'unsigned' },
			{ name = 'limitCurrency',                 type = 'map'    },
			{ name = 'limitCryptocurrency',           type = 'map'    },
			{ name = 'paymethod',                     type = 'string' },
			{ name = 'paymethodId',                   type = 'number' },
			{ name = '_data',                         type = 'map'    },
		},
		primary = {'id'},
		secondaries = {
			{ name = 'asset', parts = { 'cryptocurrency', 'type', 'rate', 'id' } },
			{ name = 'fiat', parts = { 'currency', 'type', 'rate', 'id' } },
			{ name = 'updated', parts = { 'updated_at', 'id' } },
		},
	},
}

local Order = require 'robot.Order'

function Advertise:export()
	self:fold()
	local type2trade = {
		selling = 'BUY',
		buying  = 'SELL',
	}
	return Order {
		market = 'bizlato',
		updated_at = self.updated_at,
		order_no = tostring(self.id),
		crypto = self.cryptocurrency,
		fiat = self.currency,
		price = self.rate,
		trade = type2trade[self.type],
		minFiatAmount = self.limitCurrency.min,
		maxFiatAmount = self.limitCurrency.max,
		minCryptoAmount = self.limitCryptocurrency.min,
		maxCryptoAmount = self.limitCryptocurrency.max,
		payMethods = {self.paymethod},
	}
end


local response = val.idator {
	total = 'number',
	data = val.list(validators.order),
}

return {
	response = response,
	Advertise = Advertise,
}