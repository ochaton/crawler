local function merge(t1, t2)
	if type(t1) ~= 'table' then return t2 end
	if type(t2) ~= 'table' then return t1 end

	for k in pairs(t2) do
		if type(t2[k]) ~= 'table' then
			if type(t1[k]) == 'nil' then
				t1[k] = t2[k]
			end
		else
			t1[k] = merge(t1[k], t2[k])
		end
	end
	return t1
end

return {
	merge = merge,
}