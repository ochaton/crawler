local class = require 'metaclass'

local Base = class "Robot.Base" : inherits(require 'orm.Tuple') : prototype {
}

function Base:copy(from)
	for k, v in pairs(from) do
		self[k] = v
	end
end

function Base:constructor()
	if self.validator then
		Base.copy(self, self.validator(self))
	end
	if self.dropnil then
		self:dropnil()
	end
	if not self._data then
		self:exclude()
		self:fold()
	end
end

function Base:export()
	error("You must specify :export func for: "..self:who())
end

function Base:exclude()
	self._data = self._data or setmetatable({}, {__serialize='map'})
	for key, value in pairs(self) do
		if value ~= nil and self._space.fmap[key] == nil then
			self._data[key] = value
		end
	end
	return self._data
end

function Base:dropnil()
	for k, v in pairs(self) do
		if v == nil then
			self[k] = nil
		end
	end
end

function Base:fold()
	if self.folding then
		for key, value in pairs(self) do
			if self.folding[key] then
				self[key] = self.folding[key](value)
			end
		end
	end
end

function Base:totuple()
	return self._space.tuple(self._space:totable(self))
end

function Base:tojson()
	return require 'json'.encode(self:totuple())
end

function Base:save()
	return self._space:replace(self:totuple())
end

return Base