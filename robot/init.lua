local config = require 'config'

local class = require 'metaclass'
local view = require 'robot.view'
local Robot = class "Robot" : prototype {
	q = view.q,
}

function Robot:constructor()
	self.orm = require 'robot.orm'
	if self.binance then
		self.binance = require 'robot.binance'(self.binance)
	end
	if self.bizlato then
		self.bizlato = require 'robot.bizlato'(self.bizlato)
	end
	if self.tgbot then
		self.tgbot = require 'robot.tgbot'(self.tgbot)
	end
end

function Robot:start()
	if self.tgbot then
		self.tgbot:run()
	end
end

-- singletone
return Robot(config.get('app'))
