local instance_name = assert(instance_name, "instance_name is required")

box = {
	vinyl_memory = 0,
	vinyl_cache = 0,

	memtx_dir = './data/'..instance_name,
	wal_dir = './data/'..instance_name,
	vinyl_dir = './data/'..instance_name,
}

app = {
	binance = {
		url = 'https://p2p.binance.com/bapi/c2c/v2/friendly/c2c/adv/search',
		class = 'robot.binance',

		background = {
			interval = 600,
			rps = 0.5,
			firstpage = {
				rps = 3,
				burst = 3,
			},
			pairs = {
				"RUB_USDT",
				"USD_USDT",
				"RUB_BTC",
				"USD_BTC",
				"RUB_ETH",
				"USD_ETH",
				"RUB_DAI",
				"USD_DAI",
				"RUB_BNB",
				"USD_BNB",
			},
		}
	},
}