#!/usr/bin/env tarantool
require('strict').on()
require('package.reload')

local fio = require 'fio'

local function app_root()
	local link = debug.getinfo(1, "S").source:sub(2):gsub("%.lua", "")
	local init_lua = fio.abspath(fio.readlink(link) or link)
	local root_app = fio.dirname(init_lua)
	return root_app
end

rawset(_G, 'app_root', app_root)

local function instance_name()
	return os.getenv('INSTANCE_NAME') or (
		fio.basename(debug.getinfo(1, "S").source:sub(2)):gsub("%.lua", ""))
end

local fiber = require 'fiber'
local under_tarantoolctl = fiber.name() == 'tarantoolctl'

require 'config' {
	instance_name = instance_name(),
	mkdir = true,
	file = os.getenv("CONF") or 'etc/conf.lua',
}

local robot = require('robot')
rawset(_G, 'robot', robot)
robot:start()

if not under_tarantoolctl and not fiber.self().storage.console then
	require 'console'.start()
	os.exit(0)
end
