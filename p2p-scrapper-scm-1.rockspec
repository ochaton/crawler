rockspec_format = "1.1"
package = "p2p-scrapper"
version = "scm-1"
source = {
   url = "*** please add URL for source tarball, zip or repository here ***"
}
description = {
   homepage = "*** please enter a project homepage ***",
   license = "*** please specify a license ***"
}
dependencies = {
   "tarantool >= 1.10",
   "orm",
   "config scm-5",
   "moonwalker scm-1",
   "background scm-1",
   "val scm-1",
}
build = {
   type = "builtin",
   modules = {}
}
